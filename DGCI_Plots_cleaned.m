% Yellow:HSV: 50 87 91  DGCI:  0.0178        RGB: 231 199 31 #e7c71f
% Yellow:HSV: 66 88 100 DGCI:  0.0733       RGB: 231 255 32  
% Green: HSV: 91 38 42  DGCI:  0.5722       RGB: 87 108 67  #576c43
  
%DGCI=(((50-60)/60)+(1-0.87)+(1-0.91))/3
clear all
close all
clc
prompt = {'Enter the number of first photo you want to analyze','Enter the number of last photo you want to analyze','Enter photos overall name'};
dlg_title = 'Input';
num_lines = 1;
def = {'8310','8310','DSC0'};
photo_no = inputdlg(prompt,dlg_title,num_lines,def);
mmm=str2double(photo_no{1});
nnn=str2double(photo_no{2});
rang=mmm;           % I used this "rang" to put outputs in Excel file in order.
for k=mmm:nnn
% jpgFilename= strcat('D:\Image Processing\2015 data collection\Images\Leonard 13th July\',photo_no{3}, num2str(k), '.jpg');
% jpgFilename= strcat('C:\Image Processing\2014 IDC\Test\',photo_no{3}, num2str(k), '.jpg');
% jpgFilename= strcat('E:\Image Processing\2014 data collection\july3_foreground\',photo_no{3}, num2str(k), '.jpg');
  jpgFilename= strcat('E:\Image Processing\2015 data collection\Images\Leonard 13th July',photo_no{3}, num2str(k), '.jpg');
I = imread(jpgFilename);
% I = imrotate(I, 90);
r=I(:,:,1);g=I(:,:,2);b=I(:,:,3);
HSV=rgb2hsv(I);
h=HSV(:,:,1);s=HSV(:,:,2);v=HSV(:,:,3);

%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%
mask = imbinarize(g);
h=h(mask>0); s=s(mask>0); v=v(mask>0);
r=r(mask>0); g=g(mask>0); b=b(mask>0);

DGCI = (((h*360-60)/60)+(1-s)+(1-v))/3;
DGCI_std = std(DGCI);
DGCI = mean(DGCI);
ExG = mean(2*g-r-b);
ExR = mean(1.3*r-g);
GB=mean(g-b);
RG=mean(r-g);
GR=mean(g-r);                                   %No good results
GBRG=mean(GB./RG);
CIVE=mean(0.441*r-0.811*g+0.385*b+18.78745);
ExGR=ExG-ExR;
NGRDI=mean((r-g)./(g+r));                         %the original is (g-r)./(g+r);      No result
COM1=mean(ExG+CIVE+ExGR);
MExG=mean(1.262*g-0.884*r-0.311*b);
GRAY=mean(0.2898 * r + 0.5870*g + 0.1140*b);
RBI=mean((r-b)./(r+b));
ERI=mean((r-g).*(r-b));
EGI=mean(GR.*GB);
EBI=mean((b-g).*(b-r));
EVI=mean((2.5*(g-r))./(g+6*r-7.5*b+1));
% VEG=mean(g./((r.^0.667).*(b.^0.333)));          %I could not make it work
NDI=mean((((g-r)./(g+r))+1)*128);               %No good result
NDI2=mean((((g-r)./(g+r))+1)*128);              %No good result

Range=strcat('A', num2str(k-rang+1));
input=[DGCI, DGCI_std, ExG, ExR, GB, RG, GR, GBRG,CIVE,ExGR,NGRDI,COM1,MExG,GRAY,RBI,ERI,EGI,EBI,EVI,NDI,NDI2,k];

xlswrite('DGCI_plots.xls',input,'Sheet1',Range);


end

