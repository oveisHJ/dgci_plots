function [c1,c2,c3,c4,c5,canopy] = DGCI_classes(DGCI)

canopy = length(DGCI(DGCI>0));
c1 = length(DGCI(DGCI>0   & DGCI<=0.2));
c2 = length(DGCI(DGCI>0.2 & DGCI<=0.4));
c3 = length(DGCI(DGCI>0.4 & DGCI<=0.6));
c4 = length(DGCI(DGCI>0.6 & DGCI<=0.8));
c5 = length(DGCI(DGCI>0.8 & DGCI<=1.0));

end