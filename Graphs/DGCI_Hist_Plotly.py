import numpy as np
import pandas as pd
import plotly.graph_objects as go


df = pd.read_csv('17July.csv')
df = df.dropna().reset_index(drop=True)
df1 = df[['DGCI_l', 'hue_l', 'Correct_DGCI_l', 'max_DGCI',
       ' std_DGCI', 'Corrected_hue_l','Red', 'Green', 'Blue', 'c1', 'c2', 'c3', 'c4', 'c5',
       'canopy', 'k', 'rate']].copy()

colors= ['#c5d627','#b3d627','#9fd627','#90d627',
         '#7fd627','#6ad627','#56d627','#47d627',
         '#43bd28','#3fab27','#3b9c25','#388c24']

x=df.loc[df['merged_rating']==4,'Correct_DGCI_l']

fig = go.Figure()

fig.add_trace(go.Histogram(x=np.array(x),
                                marker_color=colors,
                                xbins=dict(
                                    start=0.1,
                                    end=0.42,
                                    size=0.02
                                            ),
                                autobinx=False
                                  
                                  ))

fig.update_layout(
    autosize=True,
    width=600,
    height=500,
    yaxis_title_text = 'Frequency'

    )
fig.update_xaxes(range=[0.18, 0.42],tickvals=np.arange(0.2,0.42,0.02))    
fig.update_yaxes(range=[0, 16])

fig.show()
