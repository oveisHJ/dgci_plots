import plotly.express as px
import plotly.graph_objects as go

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#========================================================================
#f1-score plot
"""
df = pd.read_csv('./Performance.csv')
df.head()
df.dropna(inplace=True)
df.shape
df1=df[(df.Date==1) & (df.Performance_Type=='f1-score')]
df2=df[(df.Date==2) & (df.Performance_Type=='f1-score')]


fig = px.bar(df2, x="Model", y="Value", barmode="group",
             facet_col="Class",color='Model')

# Setting of bar plots in plotly
# https://plot.ly/python/bar-charts/#customizing-individual-bar-base
fig.update_traces(width=1)


fig.update_layout(
    autosize=True,
    width=700,
    height=500,
        yaxis = go.layout.YAxis(
        tickmode = 'linear',
        tick0 = 0.0,
        dtick = 0.1
))
fig.show()
"""
df1 = pd.read_csv('./03July.csv')
df2 = pd.read_csv('./17July.csv')

df1 = df1.dropna().reset_index(drop=True)
df2 = df2.dropna().reset_index(drop=True)

df1['day'] = 1
df2['day'] = 2
df1_columns=set(df1.columns)
df2_columns=set(df2.columns)
common_columns = list(df1_columns.intersection(df2_columns))

df = pd.concat([df1[common_columns],df2[common_columns]])
Diff = df1[['Plot Number','rate','Correct_DGCI_l']]
Diff = pd.merge(Diff, df2[['Plot Number','rate','Correct_DGCI_l']], on='Plot Number',suffixes=('_1','_2'))
Diff['diff_rate'] = Diff.rate_2 - Diff.rate_1
Diff['diff_dgci'] = Diff.Correct_DGCI_l_2 - Diff.Correct_DGCI_l_1
#========================================================================
#rate_diff
"""
x = sorted(Diff.diff_rate.unique())
y = Diff.diff_rate.value_counts().sort_index()
y = [i for i in y]

colors = ['#E09C00','#FFF700','#DDE800','#BBD900','#97FF00','#5BCF00','#449A00']

fig = go.Figure(data=[go.Bar(
            x=x, y=y,
            text=y,
            marker_color=colors
        )])


fig.update_layout(
    autosize=True,
    width=700,
    height=500,
    yaxis=dict(
        title='Frequency'
    ),
    xaxis=dict(
        title='Differences between IDC rating for both dates'
    ),

)

fig.show()
"""
#========================================================================
#rate_diff
colors= ['#c5d627','#b3d627','#9fd627','#90d627',
         '#7fd627','#6ad627','#56d627','#47d627',
         '#43bd28','#3fab27','#3b9c25','#388c24']
fig = go.Figure(data=[go.Histogram(x=Diff.diff_dgci,marker_color=colors)])
fig.update_layout(
    autosize=True,
    width=600,
    height=500,
    yaxis=dict(
        title='Frequency'
    ),
    xaxis=dict(
        title='Differences between average DGCI of both dates'
    )
    
)

fig.show()
