% Yellow:HSV: 50 87 91  DGCI:  0.0178        RGB: 231 199 31 #e7c71f
% Yellow:HSV: 66 88 100 DGCI:  0.0733       RGB: 231 255 32  
% Green: HSV: 91 38 42  DGCI:  0.5722       RGB: 87 108 67  #576c43
  
%DGCI=(((50-60)/60)+(1-0.87)+(1-0.91))/3
clear all
close all
clc
prompt = {'Enter the number of first photo you want to analyze','Enter the number of last photo you want to analyze','Enter photos overall name'};
dlg_title = 'Input';
num_lines = 1;
def = {'8335','8335','DSC0'};
photo_no = inputdlg(prompt,dlg_title,num_lines,def);
mmm=str2double(photo_no{1});
nnn=str2double(photo_no{2});
rang=mmm;           % I used this "rang" to put outputs in Excel file in order.
for k=mmm:nnn
% jpgFilename= strcat('D:\Image Processing\2015 data collection\Images\Leonard 13th July\',photo_no{3}, num2str(k), '.jpg');
% jpgFilename= strcat('C:\Image Processing\2014 IDC\Test\',photo_no{3}, num2str(k), '.jpg');
jpgFilename= strcat('E:\Image Processing\2014 data collection\IDC Dr ted field July17\',photo_no{3}, num2str(k), '.jpg');
%  jpgFilename= strcat('E:\Image Processing\2015 data collection\Images\Leonard 13th July\',photo_no{3}, num2str(k), '.jpg');
I = imread(jpgFilename);
I = imrotate(I, 90);
[mask,m1,m2,nStat,all_rows]=Row_Detection(I);           % Create a mask for desired plots
r=I(:,:,1);g=I(:,:,2);b=I(:,:,3);
HSV=rgb2hsv(I);
h=HSV(:,:,1);s=HSV(:,:,2);v=HSV(:,:,3);
%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%
% Detecting Pink Board
vrange=0.82;                    % To detect the pink board because it is the brightest thing in the picture
r(v<vrange)=0;g(v<vrange)=0;b(v<vrange)=0;
pic=cat(3,r,g,b);

bw=im2bw(pic,0.25);
% imshow(bw),figure
bw=imfill(bw,'holes');
bw=imopen(bw,strel('square',120));


stat=regionprops(bw,'Area','BoundingBox');  % After imopen, the pink board is the only object in the image. I used this property to crop the pink board for the faster operation
bw=imcrop(bw,stat.BoundingBox);
% ==============================================
% For publication figures
r=I(:,:,1);g=I(:,:,2);b=I(:,:,3);
r=imcrop(r,stat.BoundingBox);
g=imcrop(g,stat.BoundingBox);
b=imcrop(b,stat.BoundingBox);
r(bw==0)=0;g(bw==0)=0;b(bw==0)=0;
pinkBoard=cat(3,r,g,b);
pinkBoard=imrotate(pinkBoard,-90);
% Cropped binary pink board
imshow(pinkBoard)
axis=regionprops(bw,'MajorAxisLength','MinorAxisLength');
AREA=axis.MajorAxisLength.*axis.MinorAxisLength;
%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%
%Detecting Yellow Disk
pink=imcrop(I,stat.BoundingBox);            % Cropped RGB pink board

r=pink(:,:,1);g=pink(:,:,2);b=pink(:,:,3);
Hpink=imcrop(h,stat.BoundingBox);
Spink=imcrop(s,stat.BoundingBox);
Vpink=imcrop(v,stat.BoundingBox);

r(Hpink>0.3 | Spink<0.1)=0;g(Hpink>0.3 | Spink<0.1)=0;b(Hpink>0.3 | Spink<0.1)=0;


pic=cat(3,r,g,b);
picBW=im2bw(pic,graythresh(pic));

picBWY=imerode(picBW,strel('disk',round(AREA/80000)));      % I used this ratio based on AREA to use with different picture sizes


%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%
% Calculating DGCI Yellow
hueY=Hpink*360; satY=Spink; briY=Vpink;
hueY(picBWY==0)=0; satY(picBWY==0)=0; briY(picBWY==0)=0;       % Change all HSV values to zero except the yellow disk

% first I did "sat_yy=(1-satY)" then I used "sat_yy(satY==0)=0" because the  values of HSV matrix other than yellow disk were zero before the
% subtraction, and after the subtraction, those values are now negetive, so  with "sat_yy(satY==0)=0" the subtraction just affects on yellow disk  pixels.

sat_yy=(1-satY); sat_yy(satY<=0)=0; bri_yy=(1-briY); bri_yy(briY<=0)=0; hue_yy=(hueY-60); hue_yy(hueY<=0)=0;
DGCI=(hue_yy/60+sat_yy+bri_yy)/3;
DGCI_y=(sum(sum(DGCI)))/nnz(hueY);
satY=sum(satY(:))/nnz(satY); briY=sum(briY(:))/nnz(briY); hueY=sum(hueY(:))/nnz(hueY);
% %---------------------------------------------------------------------------------------------------------%
% %---------------------------------------------------------------------------------------------------------%
% Detecting Green Disk
r=pink(:,:,1);g=pink(:,:,2);b=pink(:,:,3);
r(Hpink<0.2 | Hpink>0.5 | Vpink>0.9)=0; g(Hpink<0.2 | Hpink>0.5 | Vpink>0.9)=0; b(Hpink<0.2 | Hpink>0.5 | Vpink>0.9)=0;
pic=cat(3,r,g,b);
picBW=im2bw(pic,graythresh(pic));
statG=regionprops(picBW,'Area');
StatG=bwconncomp(picBW);
gArea=[statG.Area];
[~,Id]=max(gArea);
picBW=false(size(picBW));
picBW(StatG.PixelIdxList{Id})=true;
% figure, imshow (picBW), title "Green"
% figure
% ===================================================
% publication figures
rp=pinkBoard(:,:,1);gp=pinkBoard(:,:,2);bp=pinkBoard(:,:,3);
picBW = imrotate(picBW,-90);
rp(picBW==0)=0;gp(picBW==0)=0;bp(picBW==0)=0;
greenDisk = cat(3,rp,gp,bp);

rp=pinkBoard(:,:,1);gp=pinkBoard(:,:,2);bp=pinkBoard(:,:,3);
picBWY = imrotate(picBWY,-90);
rp(picBWY==0)=0;gp(picBWY==0)=0;bp(picBWY==0)=0;
yellowDisk = cat(3,rp,gp,bp);

imshow(yellowDisk)
%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%
% Calculating DGCI Green
hueG=Hpink*360; satG=Spink; briG=Vpink;
hueG(picBW==0)=0; satG(picBW==0)=0; briG(picBW==0)=0;
sat_gg=(1-satG); sat_gg(picBW<=0)=0; bri_gg=(1-briG); bri_gg(picBW<=0)=0; hue_gg=(hueG-60); hue_gg(picBW<=0)=0;
DGCI=(hue_gg/60+sat_gg+bri_gg)/3;
DGCI_g=(sum(sum(DGCI)))/nnz(hueG);
satG=sum(satG(:))/nnz(satG); briG=sum(briG(:))/nnz(briG); hueG=sum(hueG(:))/nnz(hueG);

%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%
% Removing Soil
r=I(:,:,1);g=I(:,:,2);b=I(:,:,3);
a=(b-g)./(b-r); % somehow it has a good effect on removing soil
diff=g-b;   % green - blue
Q=b>210;   %blue > 210
x=20;       % limit for diff
r(a>0 | diff<x | Q)=0; g(a>0 | diff<x | Q)=0; b(a>0 | diff<x | Q)=0;

ind=2*g-r-b;
indBW=im2bw(ind,0.18);
indBW=imclose(indBW,strel('disk',40));
r(indBW==0)=0;g(indBW==0)=0;b(indBW==0)=0;
soil = indBW;
r(mask==0)=0;g(mask==0)=0;b(mask==0)=0;      % RGB values for region other than mask turn into zero 
pic=cat(3,r,g,b);
% imshow(pic),title (k)
% WritejpgFilename= strcat('E:\Image Processing\2015 data collection\Images\13thJuly2015_foreground\',photo_no{3}, num2str(k), '.jpg');
% imwrite(pic,WritejpgFilename)
%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%
% R,G,B Output to excel file

Red=pic(:,:,1);
Red=sum(Red(:))/nnz(Red);
Green=pic(:,:,2);
Green=sum(Green(:))/nnz(Green);
Blue=pic(:,:,3);
Blue=sum(Blue(:))/nnz(Blue);
%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%
HSV=rgb2hsv(pic);
H=HSV(:,:,1);S=HSV(:,:,2);B=HSV(:,:,3);

%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%% Calculatin Leaf's DGCI
% Calculating DGCI for plots
hue_l=360*H; sat_l=S; bri_l=B;
sat_ll=(1-sat_l); sat_ll(mask==0 | sat_ll<0)=0;
bri_ll=(1-bri_l); bri_ll(mask==0 | bri_ll<0)=0;
hue_ll=(hue_l-60); hue_ll(mask==0 | hue_ll<0)=0;
DGCI=(hue_ll/60+sat_ll+bri_ll)/3;
DGCI(hue_l<=0)=0;
DGCI_l=(sum(sum(DGCI)))/nnz(hue_l);


bri_l=sum(bri_l(:))/nnz(bri_l);
sat_l=sum(sat_l(:))/nnz(sat_l);

%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%
% Calculating Corrected Leaf DGCI and HUE

Slope=0.5544/(DGCI_g-DGCI_y);
Yintercept=0.0178-(Slope*DGCI_y);
Correct_DGCI_l=Slope*DGCI+Yintercept;
Correct_DGCI_l(hue_l<=0)=0;
%%
[c1,c2,c3,c4,c5,canopy] = DGCI_classes(Correct_DGCI_l);
std_DGCI = std(Correct_DGCI_l(Correct_DGCI_l>0));
max_DGCI = max(Correct_DGCI_l(:));
Correct_DGCI_l=(sum(sum(Correct_DGCI_l)))/nnz(hue_l);

Slope_hue=41/(hueG-hueY);
Yintercept_hue=91-(Slope_hue*hueG);
Corrected_hue_l=(Slope_hue)*(hue_l)+(Yintercept_hue);
Corrected_hue_l=sum(Corrected_hue_l(:))/nnz(hue_l);
hue_l=sum(hue_l(:))/nnz(hue_l);
%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%

k
 Range=strcat('A', num2str(k-rang+1));
 input=[DGCI_y,hueY,satY,briY,DGCI_g,hueG,satG,briG,DGCI_l,hue_l,sat_l,bri_l,Correct_DGCI_l,max_DGCI, std_DGCI, Corrected_hue_l,Slope,Slope_hue,Yintercept,Yintercept_hue,Red,Green,Blue,c1,c2,c3,c4,c5,canopy,k];

%  xlswrite('Krista.xls',input,'Sheet1',Range);


end

