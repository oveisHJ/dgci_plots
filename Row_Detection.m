function [Rows,m1,m2,nStat,all_rows] = Row_Detection(I)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%     I = imread('D:\Image Processing\2014 data collection\IDC dr ted field_july3\DSC08027.jpg');
    r=I(:,:,1);g=I(:,:,2);b=I(:,:,3);
    HSV=rgb2hsv(I);
    v=HSV(:,:,3);
    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
    % Removing Soil
    a=(b-g)./(b-r); % somehow it has a good effect on removing soil
    diff=g-b;   % green - blue
    Q=b>210;   %blue > 210
    x=25;       % limit for diff
    r(a>0 | diff<x | Q)=0; g(a>0 | diff<x | Q)=0; b(a>0 | diff<x | Q)=0;
    ind=2*g-r-b;
    indBW=im2bw(ind,0.18);
    indBW=imerode(indBW,strel('disk',3));
    indBW=imdilate(indBW,strel('disk',30));
    indBW=bwareaopen(indBW,40000);
    all_rows = indBW;
%     imshow(indBW),title 1
    statRows=regionprops(indBW,'Centroid','PixelIdxList','Orientation');

    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
    vrange=0.82;                    % To detect the pink board because it is the brightest thing in the picture
    b=I(:,:,3);
    b(v<vrange)=0;

    bw=im2bw(b);
    bw=imfill(bw,'holes');
    bw=imopen(bw,strel('square',80));
    bw=bwareaopen(bw,10000);
    statPink=regionprops(bw,'Centroid');
    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
     section=25;                       %width of each section
    for i=1000:section:2000
        indbw=indBW(:,i:section+i);    %creating a section of image
        indbw=imdilate(indbw,strel('disk',120));
        stat=regionprops(indbw,'Centroid');
        statSize=size(stat,1);
        if statSize>=2
                break
        end
    end
        findCenter=zeros(1,statSize);
        for j=1:statSize
            findCenter(j)=stat(j).Centroid(2)-statPink(1).Centroid(2);
        end   
        for mid=1:2
%             Finding the minimum of the centroids and switching the values
%             with 999999 because the loop goes over the findCenter again.
            [~,id]=min(abs(findCenter));
            nStat(mid)=stat(id);
            nStat(mid).Centroid(1)=nStat(mid).Centroid(1)+i;
            findCenter(id)=99999;
        end
    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
    section=25;                       %width of each section
    for i=2500:-1*section:1500
        indbw=indBW(:,i-section:i);    %creating a section of image
%         Plots are almost horizontal because we rotated the image.
         indbw=imdilate(indbw,strel('disk',120));
        stat=regionprops(indbw,'Centroid');
        statSize=size(stat,1);
        if statSize>=2
                break
        end
    end
        findCenter=zeros(1,statSize);
        for j=1:statSize
            findCenter(j)=stat(j).Centroid(2)-statPink(1).Centroid(2);
        end   
        for mid=3:4
            [~,id]=min(abs(findCenter));
            nStat(mid)=stat(id);
            nStat(mid).Centroid(1)=nStat(mid).Centroid(1)+i-75;
            findCenter(id)=99999;
        end

    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
    %Altering Matlab because sometimes it put the greater Yvalue of two points in 2nd place
    %nStat(5) is only a placeholder for swaping the values
    if nStat(3).Centroid(2)<nStat(4).Centroid(2)
        nStat(5).Centroid(2)=nStat(3).Centroid(2);
        nStat(3).Centroid(2)=nStat(4).Centroid(2);
        nStat(4).Centroid(2)=nStat(5).Centroid(2);
    end
    if nStat(1).Centroid(2)<nStat(2).Centroid(2)
        nStat(5).Centroid(2)=nStat(1).Centroid(2);
        nStat(1).Centroid(2)=nStat(2).Centroid(2);
        nStat(2).Centroid(2)=nStat(5).Centroid(2);
    end
    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
    %Drawing two centerlines and finding slope of two lines
    m1=(nStat(3).Centroid(2)-nStat(1).Centroid(2))/(nStat(3).Centroid(1)-nStat(1).Centroid(1));
    m2=(nStat(4).Centroid(2)-nStat(2).Centroid(2))/(nStat(4).Centroid(1)-nStat(2).Centroid(1));

    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
    % Detecting rows
    Rows=false(size(indBW));
    for obj=1:size(statRows,1)
%         y2 = m(x2-x1)+y1
%         Calculating the y of centerpoints of all possible plot rows.
        Yline=round(m1*(statRows(obj).Centroid(1)-nStat(3).Centroid(1))+ nStat(3).Centroid(2));
%         if the distance of calculated y of centerpoint was less than 200
%         pixel away from its actual y, then it is one of the middle rows.
        if abs(Yline-statRows(obj).Centroid(2))<200
            Rows(statRows(obj).PixelIdxList)=true;
        end
    end
    for obj=1:size(statRows,1)
        Yline=round(m2*(statRows(obj).Centroid(1)-nStat(4).Centroid(1))+ nStat(4).Centroid(2));
        if abs(Yline-statRows(obj).Centroid(2))<200
            Rows(statRows(obj).PixelIdxList)=true;
        end
    end

    stat=regionprops(Rows,'Orientation','PixelIdxList');
    for z=1:size(stat,1)
        if abs(stat(z).Orientation)>65
            Rows(stat(z).PixelIdxList)=false;
        end
    end


end

