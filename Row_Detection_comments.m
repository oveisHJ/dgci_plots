    I = imread('C:\Image Processing\2014 data collection\IDC dr ted field_july3\DSC08032.jpg');
    I = imrotate(I, 90);
    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
lab = rgb2lab(I);
a = lab(:,:,2);
a = uint8(a+127);
board = a;
board(a<150) = 0;
board = im2bw(board);
board = imfill(board,'holes');
board = bwareaopen(board,10000);
statPink=regionprops(board,'Centroid');
imshow(board)

%---------------------------------------------------------------------------------------------------------%
%---------------------------------------------------------------------------------------------------------%
%% Removing soil and detecting plots
bg = a;
bg(board==1) = mean(mean(a));
bg = ~im2bw(bg, graythresh(bg));
bg=imerode(bg,strel('disk',3));
bg=imdilate(bg,strel('disk',30));
indBW=bwareaopen(bg,40000);
imshow(bg)

%%    
    imshow(indBW),title 1
    statRows=regionprops(indBW,'Centroid','PixelIdxList','Orientation');
%%

    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
     section=25;                       %width of each section
    for i=1000:section:2000
        indbw=indBW(:,i:section+i);    %creating a section of image
        
        indbw=imdilate(indbw,strel('disk',120));
        imshow(indbw)
        stat=regionprops(indbw,'Centroid');
        statSize=size(stat,1);
        if statSize>=2
                break
        end
    end
        findCenter=zeros(1,statSize);
        for j=1:statSize
            findCenter(j)=stat(j).Centroid(2)-statPink(1).Centroid(2);
        end   
        for mid=1:2
            [~,id]=min(abs(findCenter));
            nStat(mid)=stat(id);
            nStat(mid).Centroid(1)=nStat(mid).Centroid(1)+i;
            findCenter(id)=99999;
        end
        
%%      
    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
    section=25;                       %width of each section
    for i=2500:-1*section:1500
        indbw=indBW(:,i-section:i);    %creating a section of image
        imshow(indbw)
        indbw=imdilate(indbw,strel('disk',120));
        stat=regionprops(indbw,'Centroid');
        statSize=size(stat,1);
        if statSize>=2
                break
        end
    end
        findCenter=zeros(1,statSize);
        for j=1:statSize
            findCenter(j)=stat(j).Centroid(2)-statPink(1).Centroid(2);
        end   
        for mid=3:4
            [~,id]=min(abs(findCenter));
            nStat(mid)=stat(id);
            nStat(mid).Centroid(1)=nStat(mid).Centroid(1)+i-75;
            findCenter(id)=99999;
        end

    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
    %Altering Matlab because sometimes it put the greater Yvalue of two points in 2nd place
    if nStat(3).Centroid(2)<nStat(4).Centroid(2)
        nStat(5).Centroid(2)=nStat(3).Centroid(2);
        nStat(3).Centroid(2)=nStat(4).Centroid(2);
        nStat(4).Centroid(2)=nStat(5).Centroid(2);
    end
    if nStat(1).Centroid(2)<nStat(2).Centroid(2)
        nStat(5).Centroid(2)=nStat(1).Centroid(2);
        nStat(1).Centroid(2)=nStat(2).Centroid(2);
        nStat(2).Centroid(2)=nStat(5).Centroid(2);
    end
    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
    %Drawing two centerlines and finding slope of two lines
    m1=(nStat(3).Centroid(2)-nStat(1).Centroid(2))/(nStat(3).Centroid(1)-nStat(1).Centroid(1));
    m2=(nStat(4).Centroid(2)-nStat(2).Centroid(2))/(nStat(4).Centroid(1)-nStat(2).Centroid(1));

    %---------------------------------------------------------------------------------------------------------%
    %---------------------------------------------------------------------------------------------------------%
    % Detecting rows
    Rows=false(size(indBW));
    for obj=1:size(statRows,1)
        Yline=round(m1*(statRows(obj).Centroid(1)-nStat(3).Centroid(1))+ nStat(3).Centroid(2));
        if abs(Yline-statRows(obj).Centroid(2))<200
            Rows(statRows(obj).PixelIdxList)=true;
        end
    end
    for obj=1:size(statRows,1)
        Yline=round(m2*(statRows(obj).Centroid(1)-nStat(4).Centroid(1))+ nStat(4).Centroid(2));
        if abs(Yline-statRows(obj).Centroid(2))<200
            Rows(statRows(obj).PixelIdxList)=true;
        end
    end

    stat=regionprops(Rows,'Orientation','PixelIdxList');
    for z=1:size(stat,1)
        if abs(stat(z).Orientation)>65
            Rows(stat(z).PixelIdxList)=false;
        end
    end
